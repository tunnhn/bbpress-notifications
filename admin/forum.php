<?php
/*
Class Name: bbNotificationsAdminForum
*/
class bbNotificationsAdminForum{
	
	/**
	 * @var bbNotifications
	 */
	protected static $_instance = null;


	function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_box_data' ) );
	}

	/**
	 * Add meta box
	 */
	function add_meta_boxes() {
		add_meta_box(
			'bbpn_sectionid'
			, __( 'bbPress Notification Settings', 'bbpress-notifications' )
			, array( $this, 'meta_box_callback' )
			, 'forum'
			, 'side'
			, 'high' 
		);
	}

	/**
	 * Assign in forum
	 *
	 * @param $post
	 */
	function meta_box_callback( $post ) {
		$values = get_post_meta( $post->ID, 'bbpress-notifications-mods', true );
		$args      = array(
			'role' => 'bbp_moderator'
		);
		$args_key  = array(
			'role' => 'bbp_keymaster'
		);
		$users_mod = get_users( $args );
		$users_key = get_users( $args_key );
		$mods      = array_merge( $users_mod, $users_key );
		?>
		<div class="inside">
			<p>
				<strong><?php echo __( 'Moderators', 'tps' ); ?></strong>
				<select name="bbpress-notifications-mods[]" multiple="true" class="chosen-select">
					<?php
					if ( count( $mods ) > 0 ) {
						foreach ( $mods as $mod ) {
							$selected = '';
							if ( is_array( $values ) ) {
								if ( in_array( $mod->ID, $values ) ) {
									$selected = 'selected = "selected"';
								}
							}
							?>
							<option value="<?php echo $mod->ID ?>" <?php echo $selected; ?>><?php echo $mod->data->display_name ?></option>
						<?php
						}
					} ?>
				</select>
			</p>
		</div>
	<?php
	}

	/**
	 * Save data in meta box
	 */
	function save_meta_box_data( $post_id ) {
		// Check the user's permissions.
		if ( current_user_can( 'edit_post', $post_id ) || current_user_can( 'moderate' ) ) {
			if ( get_post_type() == 'forum' ) {
				$bbpress_notifications_mods = $_POST['bbpress-notifications-mods'];
				update_post_meta( $post_id, 'bbpress-notifications-mods', $bbpress_notifications_mods );
			}
		}
	}
	
	public static function instance() {
		if ( !self::$_instance ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
}

bbNotificationsAdminForum::instance();
?>