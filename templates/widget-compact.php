<?php
$data = bbn_get_user_notifications();

$notifications = isset($data['notifications'])?$data['notifications']:array();
$rows = isset($data['rows'])?$data['rows']:array();
$notifications_number = count($rows);
?>

<ul>
	<li class="mini-bbn-notifications-hover">
		<div>
			<span class="dashicons dashicons-email-alt"></span>
			<span class="items-number"><?php echo $notifications_number;?></span>
			<div class="clear"></div>
		</div>
		<div class="widget_bbn_content">
			<ul class="bbn-notifications">
<?php
	if ( !empty( $rows ) ) {
		foreach ( $rows as $row ):
			$reply_id = 0;
			$item_link = '';
			if($row->post_type=='topic'){
				$item_link = get_the_permalink( $row->ID );
			} else {
				$item_link = get_the_permalink( $row->post_parent );
				$item_link .= '#post-' . $row->ID;
			}
//			if ( isset( $notifications[$row->ID]['r'] ) && $notifications[$row->ID]['r'] ) {
//				$reply_id = $notifications[$row->ID]['r'];
//				$item_link .= '#post-' . $reply_id;
//			}
			?>
				<li id="bbn-notification-<?php echo $row->ID ?>" class="bbn-notification-row">
					<?php echo get_avatar( $row->post_author, 64 ); ?>
					<div class="bbn-notification-content">
						<h3 class="bbn-title"><a href="<?php echo $item_link; ?>"><?php echo get_the_title($row->ID); ?></a></h3>
						<div class="bbn-text">
						<?php echo bbn_trim_content( $row->post_content ); ?>
						</div>
						<div class="bbn-notify-action"><a href="#"><?php _e('mark as read','bbpress-notifications');?></a></div>
					</div>
				</li>
			<?php
		endforeach;
	} else {
?>
				<li class="empty"><?php _e('No new message ^^', 'bbpress-notifications'); ?></li>
<?php
	}
?>
			</ul>
		</div>
	</li>
</ul>