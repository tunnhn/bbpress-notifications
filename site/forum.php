<?php
/*
Class Name: bbNotificationsAdminForum
*/
class bbNotificationsSiteForum{
	
	/**
	 * @var bbNotifications
	 */
	protected static $_instance = null;
	protected $mods = array();

	function __construct() {
		add_action( 'bbp_new_topic', array( $this, 'add_new_topic_notification' ), 10, 4 );
		add_action( 'bbp_new_reply', array( $this, 'add_new_reply_notification' ), 10, 5 );
	}

	/**
	 * Create reply notify for mod user
	 *
	 * @param type $reply_id
	 * @param type $topic_id
	 * @param type $forum_id
	 * @param type $anonymous_data
	 * @param type $reply_author
	 */
	public function add_new_topic_notification( $topic_id = 0, $forum_id = 0, $anonymous_data = false, $topic_author = 0 ) {
		if ( is_user_logged_in() ) {
			if ( $forum_id ) {
				$mods = $this->get_mods_of_forum( $forum_id );
				if ( is_array( $mods ) && !empty( $mods ) ) {
					foreach ( $mods as $mod ) {
						if( $mod == $reply_author ) {
							continue;
						}
						$notifications = get_user_meta( $mod, 'bbpress-notifications', true );
						if ( !$notifications || !is_array( $notifications ) ) {
							$notifications = array();
						}
						if(!isset($notifications[$topic_id])){
							$notifications[$topic_id] = array( 'r' => '','t' => $topic_id, 'f' => $forum_id, 's'=> 0 );
						}
						update_user_meta( $mod, 'bbpress-notifications', $notifications );
					}
				}
			}
		}
	}

	/**
	 * Create reply notify for mod user
	 *
	 * @param type $reply_id
	 * @param type $topic_id
	 * @param type $forum_id
	 * @param type $anonymous_data
	 * @param type $reply_author
	 */
	public function add_new_reply_notification( $reply_id = 0, $topic_id = 0, $forum_id = 0, $anonymous_data = false, $reply_author = 0 ) {
		if ( is_user_logged_in() ) {
			if ( $forum_id ) {
				$mods = $this->get_mods_of_forum( $forum_id );
				if ( is_array( $mods ) && !empty( $mods ) ) {
					foreach ( $mods as $mod ) {
						if( $mod == $reply_author ) {
							continue;
						}
						$notifications = get_user_meta( $mod, 'bbpress-notifications', true );
						if ( !$notifications || !is_array( $notifications ) ) {
							$notifications = array();
						}
						$notifications[$reply_id] = array( 'r' => $reply_id,'t' => $topic_id, 'f' => $forum_id, 's'=> 0 );
//						if ( !isset($notifications[$topic_id]) ) {
//							$notifications[$topic_id] = array( 'r' => $reply_id,'t' => $topic_id, 'f' => $forum_id, 's'=> 0 );
//						} else {
//							$notifications[$topic_id]['r'] = $reply_id;
//						}
						update_user_meta( $mod, 'bbpress-notifications', $notifications );
					}
				}
			}
		}
	}

	private function get_mods_of_forum( $forum_id ) {
		if ( !isset( $this->mods[$forum_id] ) ) {
			$mods = get_post_meta( $forum_id, 'bbpress-notifications-mods', true );
			if ( is_array( $mods ) && !empty( $mods ) ) {
				$this->mods[$forum_id] = $mods;
			}
		}
		return isset( $this->mods[$forum_id] ) ? $this->mods[$forum_id] : array();
	}
	
	public static function instance() {
		if ( !self::$_instance ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
}

bbNotificationsSiteForum::instance();
?>