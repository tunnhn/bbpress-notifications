<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function bbn_get_user_notifications( $user_id = null ) {
	global $wpdb;
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$notifications = get_user_meta( $user_id, 'bbpress-notifications', true );
	if ( $notifications && is_array( $notifications ) ) {
		$ids = array_keys( $notifications );
		if ( empty( $ids ) ) {
			return false;
		}
		$ids_str = implode( ',', $ids );
		$sql = "SELECT * FROM {$wpdb->posts} p "
				. " WHERE `p`.`post_status`='publish' "
				. " AND (`p`.`post_type`='topic' OR `p`.`post_type`='reply' ) "
				. " AND `p`.`ID` IN({$ids_str})"
				. " ORDER BY `p`.`post_date` DESC";
		$rows = $wpdb->get_results( $sql );
		return array('notifications' => $notifications, 'rows' => $rows);
	}
	return false;
}

function bbn_trim_content( $content, $len = 50 ) {
	$content = strip_tags( $content );
	$words = preg_split( '~\s+~', $content );
	if ( sizeof( $words ) <= $len ) {
		return $content;
	}
	$words = array_slice( $words, 0, $len );
	$words = join( ' ', $words );
	return $words;
}
