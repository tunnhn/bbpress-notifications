<?php
/*
Plugin Name: bbPress Notifications
Plugin URI: http://localhost
Description: This plugin will send notify popup to mod user when have new topic or reply is created on their forum.
Author: Phong Lo & Tu Nguyen
Version: 0.1
Author URI: http://localhost
Tags: bbpress, notifications, notify
Text Domain: bbpress-notifications
Domain Path: /languages/
*/

define('PLG_BBN_FILE', __FILE__);
define('PLG_BBN_DIR', __DIR__);
define('PLG_BBN_URL', __FILE__);
include_once "class-bbn-widget.php";
if ( !class_exists( 'bbNotifications' ) ) {

	/**
	 * Class bbNotifications
	 */
	class bbNotifications {
		/**
		 * @var bbNotifications
		 */
		protected static $_instance = null;
		protected $mods = array();

		/**
		 * bbNotifications constructor.
		 */
		public function __construct() {
			$this->includes();
//			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
//			add_action( 'bbp_new_topic', array( $this, 'add_new_topic_notification' ), 10, 4 );
//			add_action( 'bbp_new_reply', array( $this, 'add_new_reply_notification' ), 10, 5 );
//			add_action( 'wp_footer', array( $this, 'footer' ) );
//
			// ajax
//			add_action( 'wp_ajax_bbn_grab_data', array( $this, 'grab_data' ) );
		}

		public function includes() {
			require_once __DIR__.DIRECTORY_SEPARATOR.'functions.php';
			if ( is_admin() ) {
				require_once __DIR__ . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'forum.php';
			}
			// register action to create notification data
			require_once __DIR__ . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'forum.php';
			// register action to send notification to mod user
			require_once __DIR__ . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'notify.php';
		}

		public function grab_data() {
			$forum_author = get_current_user_id();
			$author_data  = get_user_meta( $forum_author, 'bbpress-notifications', true );
			if ( !$author_data ) {
				return false;
			}
			$response = array();
			foreach ( $author_data as $post_id => $data ) {
				if ( !$data ) {
					continue;
				}
				$post = get_post( $post_id );
				if ( !$post ) {
					continue;
				}
				$post_type = $post->post_type;
				if ( $post_type == bbp_get_reply_post_type() ) {
					$avatar = bbp_get_reply_author_avatar( $post->ID, '64' );
				} else {
					$avatar = bbp_get_topic_author_avatar( $post->ID, '64' );
				}
				//$topic = bbp_get_topic( $topic_id );
				$icon = '';
				if ( $avatar ) {
					if ( preg_match( '~src=\'(.*)\'~iSU', $avatar, $m ) ) {
						$icon = $m[1];
					}
				}

				$response[$post->ID] = array(
					'id'       => $post->ID,
					'title'    => get_the_title( $post->ID ),
					'content'  => $this->trim_content( apply_filters( 'the_content', $post->post_content ), 15 ),
					'forum_id' => $data['forum_id'],
					'forum'    => get_the_title( $data['forum_id'] ),
					'author'   => $post_type == bbp_get_topic_post_type() ? bbp_get_topic_author_display_name( $post->ID ) : bbp_get_reply_author_display_name( $post->ID ),
					'icon'     => $icon
				);
			}
			wp_send_json( $response );
			die();
		}

		public function trim_content( $content, $len = 50 ) {
			$content = strip_tags( $content );
			$words   = preg_split( '~\s+~', $content );
			if ( sizeof( $words ) <= $len ) {
				return $content;
			}
			$words = array_slice( $words, 0, $len );
			$words = join( ' ', $words );
			return $words;
		}

		public function footer() {
			$js = array(
				'ajax'  => admin_url( 'admin-ajax.php' ),
				'sound' => plugins_url( '/assets/notification.ogg', __FILE__ ),
				'icon'  => plugins_url( '/assets/notification.png', __FILE__ ),
			)
			?>
			<script type="text/template" id="tmpl-bbn-notification">
				<li id="bbn-notification-{{{ data.id }}}" class="bbn-notification-row">
					<# if(data.icon){ #>
						<img src="{{{data.icon}}}" />
						<# } #>
							<div class="bbn-notification-content">
								<h3 class="bbn-title">{{{ data.title }}}</h3>
								<div class="bbn-text">
									{{{ data.content }}}
								</div>
							</div>
				</li>
			</script>
			<script type="text/javascript">
				var bbnSettings = <?php echo wp_json_encode( $js );?>
			</script>
			<?php
		}

		/**
		 * Create reply notify for mod user
		 *
		 * @param type $reply_id
		 * @param type $topic_id
		 * @param type $forum_id
		 * @param type $anonymous_data
		 * @param type $reply_author
		 */
		public function add_new_topic_notification( $topic_id = 0, $forum_id = 0, $anonymous_data = false, $topic_author = 0 ) {
			if ( is_user_logged_in() ) {
				if ( $forum_id ) {
					$mods = $this->get_mods_of_forum( $forum_id );
					if ( is_array( $mods ) && !empty( $mods ) ) {
						foreach ( $mods as $mod ) {
							$notifications = get_user_meta($mod, 'bbpress-notifications', true );
							if(!$notifications){
								$notifications = array();
							}
							$notifications[$topic_id] = array('topic_id'=>$topic_id, 'forum_id'=>$forum_id, 'topic_author' => $topic_author, 'status'=>'unread');
							update_user_meta($mod, 'bbpress-notifications', $notifications);
						}
					}
				}
			}
		}

		/**
		 * Create reply notify for mod user
		 *
		 * @param type $reply_id
		 * @param type $topic_id
		 * @param type $forum_id
		 * @param type $anonymous_data
		 * @param type $reply_author
		 */
		public function add_new_reply_notification( $reply_id = 0, $topic_id = 0, $forum_id = 0, $anonymous_data = false, $reply_author = 0 ) {
			if ( is_user_logged_in() ) {
				if ( $forum_id ) {
					$mods = $this->get_mods_of_forum( $forum_id );
					if ( is_array($mods) && !empty( $mods ) ) {
						foreach ( $mods as $mod ) {
							$notifications = get_user_meta($mod, 'bbpress-notifications', true );
							if(!$notifications){
								$notifications = array();
							}
							$notifications[$reply_id] = array('topic_id'=>$topic_id, 'forum_id'=>$forum_id, 'topic_author' => $topic_author, 'status'=>'unread');
							update_user_meta($mod, 'bbpress-notifications', $notifications);
						}
					}
				}
			}
		}

		public function new_topic( $topic_id, $forum_id, $anonymous_data, $topic_author ) {
			$forum = bbp_get_forum( $forum_id );
			if ( !$forum ) {
				return;
			}
			$topic = bbp_get_topic( $topic_id );
			if ( !$topic ) {
				return;
			}

			$forum_author = $forum->post_author;

			if ( $forum_author == $topic_author ) {
				//return;
			}

			$author_data = get_user_meta( $forum_author, 'bbn_data', true );
			if ( !$author_data ) {
				$author_data = array();
			}
			if ( empty( $author_data[$forum_id] ) ) {
				$author_data[$forum_id] = array();
			}
			$author_data[$forum_id][$topic_id] = array(
				'author' => $topic_author,
				'data'   => $anonymous_data
			);

			update_user_meta( $forum_author, 'bbn_data', $author_data );
		}

		private function get_mods_of_forum( $forum_id ) {
			if ( !isset( $this->mods[$forum_id] ) ) {
				$mods = get_post_meta( $forum_id, 'bbpress-notifications-mods', true );
				if ( is_array( $mods ) && !empty( $mods ) ) {
					$this->mods[$forum_id] = $mods;
				}
			}
			return isset( $this->mods[$forum_id] ) ? $this->mods[$forum_id] : array();
		}

		/**
		 * Register styles + scripts for our plugin
		 */
		public function enqueue_assets() {
			wp_enqueue_script( 'bb-notifications-script', plugins_url( '/assets/script.js', __FILE__ ), array( 'jquery', 'backbone', 'wp-util' ) );
			wp_enqueue_script( 'bb-coice', 'https://code.responsivevoice.org/responsivevoice.js' );
			wp_enqueue_style( 'bb-notifications-style', plugins_url( '/assets/style.css', __FILE__ ) );
		}

		/**
		 * Return singleton instance of bbNotifications
		 *
		 * @return bbNotifications
		 */
		public static function instance() {
			if ( !self::$_instance ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
	}

}
// Start our plugin
bbNotifications::instance();
