<?php

if ( !class_exists( 'bbNotificationsNotify' ) ) {

	/**
	 * Class bbNotifications
	 */
	class bbNotificationsNotify {
		/**
		 * @var bbNotifications
		 */
		protected static $_instance = null;

		/**
		 * bbNotifications constructor.
		 */
		public function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
			add_action( 'wp_footer', array( $this, 'footer' ) );

			// ajax
			add_action( 'wp_ajax_bbn_grab_data', array( $this, 'grab_data' ) );
		}

		public function grab_data() {
			$forum_author = get_current_user_id();
			$author_data  = get_user_meta( $forum_author, 'bbpress-notifications', true );
			if ( !$author_data ) {
				return false;
			}
			$response = array();
			foreach ( $author_data as $post_id => $data ) {
				if ( !$data ) {
					continue;
				}
				$post = get_post( $post_id );
				if ( !$post ) {
					continue;
				}
				$post_type = $post->post_type;
				if ( $post_type == bbp_get_reply_post_type() ) {
					$avatar = bbp_get_reply_author_avatar( $post->ID, '64' );
				} else {
					$avatar = bbp_get_topic_author_avatar( $post->ID, '64' );
				}
				$icon = '';
				if ( $avatar ) {
					if ( preg_match( '~src=\'(.*)\'~iSU', $avatar, $m ) ) {
						$icon = $m[1];
					}
				}
				$response[$post->ID] = array(
					'id'       => $post->ID,
					'title'    => get_the_title( $post->ID ),
					'content'  => $this->trim_content( apply_filters( 'the_content', $post->post_content ), 15 ),
					'forum_id' => $data['forum_id'],
					'forum'    => get_the_title( $data['forum_id'] ),
					'author'   => $post_type == bbp_get_topic_post_type() ? bbp_get_topic_author_display_name( $post->ID ) : bbp_get_reply_author_display_name( $post->ID ),
					'icon'     => $icon
				);
			}
			wp_send_json( $response );
			die();
		}

		public function trim_content( $content, $len = 50 ) {
			$content = strip_tags( $content );
			$words   = preg_split( '~\s+~', $content );
			if ( sizeof( $words ) <= $len ) {
				return $content;
			}
			$words = array_slice( $words, 0, $len );
			$words = join( ' ', $words );
			return $words;
		}

		public function footer() {
			$js = array(
				'ajax'  => admin_url( 'admin-ajax.php' ),
				'sound' => plugins_url( '/assets/notification.ogg', PLG_BBN_FILE ),
				'icon'  => plugins_url( '/assets/notification.png', PLG_BBN_FILE ),
			)
			?>
			<script type="text/template" id="tmpl-bbn-notification">
				<li id="bbn-notification-{{{ data.id }}}" class="bbn-notification-row">
					<# if(data.icon){ #>
						<img src="{{{data.icon}}}" />
						<# } #>
							<div class="bbn-notification-content">
								<h3 class="bbn-title">{{{ data.title }}}</h3>
								<div class="bbn-text">
									{{{ data.content }}}
								</div>
							</div>
				</li>
			</script>
			<script type="text/javascript">
				var bbnSettings = <?php echo wp_json_encode( $js );?>
			</script>
			<?php
		}

		/**
		 * Register styles + scripts for our plugin
		 */
		public function enqueue_assets() {
			wp_enqueue_script( 'bb-notifications-script', plugins_url( '/assets/script.js', PLG_BBN_FILE ), array( 'jquery', 'backbone', 'wp-util' ) );
			wp_enqueue_script( 'bb-coice', 'https://code.responsivevoice.org/responsivevoice.js' );
			wp_enqueue_style( 'bb-notifications-style', plugins_url( '/assets/style.css', PLG_BBN_FILE ) );
		}

		/**
		 * Return singleton instance of bbNotifications
		 *
		 * @return bbNotifications
		 */
		public static function instance() {
			if ( !self::$_instance ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
	}
}

// Init notify grab
bbNotificationsNotify::instance();
