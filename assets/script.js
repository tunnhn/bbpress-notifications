;(function ($) {
	function playSound(url) {
		var audio = new Audio(url);
		audio.play();
	}

	function notifyMe(options) {
		options = $.extend({
			title: 'Hi!',
			body : "This is the body of the notification",
			icon : "icon.jpg",
			dir  : "ltr",
			sound: ''
		}, options || {});
		var show = false;
		if (!("Notification" in window)) {
			alert("This browser does not support desktop notification");
		} else if (Notification.permission === "granted") {
			show = true;
		} else if (Notification.permission !== 'denied') {
			Notification.requestPermission(function (permission) {
				if (!('permission' in Notification)) {
					Notification.permission = permission;
				}
				if (permission === "granted") {
					show = true;
				}
			});
		}
		if (!show) {
			return;
		}
		var notification = new Notification(options.title, options);
		console.log(options)
		if (options.sound) {
			playSound(options.sound);
		}

	}

	var Timer = function (_callback, _args) {
		var timer = null,
			callback = $.isFunction(_callback) ? _callback : function () {
				console.log('11111')
			},
			args = $.extend({
				interval: 10000, // 10s,
				loop    : true
			}, args || {});

		function loop() {
			callback();
			if (args.loop) {
				tick();
			}
		}

		function tick() {
			clear();
			timer = setTimeout(loop, args.interval);
			return this;
		}

		function stop() {
			clear();

		}

		function clear() {
			timer && clearTimeout(timer);
		}

		this.tick = tick;
		this.stop = stop;
		this.clear = clear;
		return this;
	}

	var bbNotifications = function (options) {
		this.model = new bbNotifications.model(options);
		this.view = new bbNotifications.view({model: this.model});
		this.model.view = this.view;
	}

	bbNotifications.model = Backbone.Model.extend({
		timer     : null,
		options   : '',
		initialize: function (options) {
			_.bindAll(this, 'start');
			this.options = options;
			this.timer = new Timer(this.start, {});
			this.timer.tick();
		},
		start     : function () {
			var that = this;
			$.ajax({
				url     : this.options.ajax,
				data    : {
					action: 'bbn_grab_data'
				},
				dataType: 'json',
				success : function (response) {
					that.view.showData(response);
				}
			});
		}
	});

	bbNotifications.view = Backbone.View.extend({
		initialize              : function () {
		},
		showData                : function (data) {
			var that = this;
			var $lists = $('.bbn-notifications');
			var hasNotification = false;
			$.each($lists, function () {
				var $list = $(this);
				_.mapObject(data, function (topic, id) {
					if ($list.find('#bbn-notification-' + id).length || that.hasDisplayedNotification(id + '')) {
						return;
					}
					var $tmpl = $(wp.template('bbn-notification')(topic));
					$list.prepend($tmpl);
					notifyMe({
						title: topic.title,
						body : topic.content,
						icon : topic.icon ? topic.icon : that.model.get('icon'),
						sound: that.model.get('sound')
					});
					that.setDisplayedNotification(id);
					hasNotification = true;
				});
			});
			if (!hasNotification) {
				return;
			}
			responsiveVoice.speak('Hi! You have a new message.');

		},
		hasDisplayedNotification: function (id) {
			var ids = ($.cookie('bbn-notifications') + '').split(',');
			return $.inArray(id, ids) !== -1;
		},
		setDisplayedNotification: function (id) {
			var ids = ($.cookie('bbn-notifications') + '').split(',');
			if ($.inArray(id, ids) == -1) {
				ids.push(id);
			}
			$.cookie('bbn-notifications', ids.join(','), {path: '/'})
		}
	});

	$(document).ready(function () {
		window.bbNotifications = new bbNotifications(bbnSettings);

	})
})(jQuery);


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch (e) {
		}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path ? '; path=' + options.path : '',
				options.domain ? '; domain=' + options.domain : '',
				options.secure ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, {expires: -1}));
		return !$.cookie(key);
	};

}));
