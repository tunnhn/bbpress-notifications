<?php

class BBN_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'bbn-widget',
			'description' => 'bbPress Notifications',
		);
		parent::__construct( 'bbn-widget', 'bbPress Notifications', $widget_ops );
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );
		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];
		echo $args['before_title'];
		echo $title;
		echo $args['after_title'];
		
		$layout = ( ! empty( $instance['layout'] ) ) ? $instance['layout'] : 'default';
		$layouts = array('default', 'compact');
		if ( !in_array( $layout, $layouts ) ) {
			$layout = 'default';
		}
		$layout_file = 'widget-' . $layout . '.php';
		
		if ( $overridden_template = locate_template( 'bbn-widget' . DIRECTORY_SEPARATOR . $layout_file ) ) {
			load_template( $overridden_template, FALSE );
		} else {
			load_template( PLG_BBN_DIR . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $layout_file, FALSE );
		}
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['layout'] = sanitize_text_field($new_instance['layout']);
		return $instance;
	}


	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$layout    = isset( $instance['layout'] ) ? esc_attr( $instance['layout'] ) : 'default';
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Layout:' ); ?></label>
		<select name="<?php echo $this->get_field_name( 'layout' ); ?>">
			<option value="default" <?php echo selected('default',$layout ); ?>><?php _e('Default'); ?></option>
			<option value="compact" <?php echo selected('compact',$layout ); ?>><?php _e('Compact'); ?></option>
		</select>
<?php
	}
}

function bbn_register_widget() {
	register_widget( 'BBN_Widget' );
}

add_action( 'widgets_init', 'bbn_register_widget' );
