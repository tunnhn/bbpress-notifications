<?php
$data = bbn_get_user_notifications();
if ( $data && is_array( $data ) ) {
	$notifications = $data['notifications'];
	$rows = $data['rows'];
	if ( !empty( $rows ) ) {
		?>
		<ul class="bbn-notifications">
		<?php
		foreach ( $rows as $row ):
			$reply_id = 0;
			$item_link = '';
			if($row->post_type=='topic'){
				$item_link = get_the_permalink( $row->ID );
			} else {
				$item_link = get_the_permalink( $row->post_parent );
				$item_link .= '#post-' . $row->ID;
			}
//			if ( isset( $notifications[$row->ID]['r'] ) && $notifications[$row->ID]['r'] ) {
//				$reply_id = $notifications[$row->ID]['r'];
//				$item_link .= '#post-' . $reply_id;
//			}
			?>
			<li id="bbn-notification-<?php echo $row->ID ?>" class="bbn-notification-row">
				<?php echo get_avatar( $row->post_author, 64 ); ?>
				<div class="bbn-notification-content">
					<h3 class="bbn-title"><a href="<?php echo $item_link; ?>"><?php echo get_the_title($row->ID); ?></a></h3>
					<div class="bbn-text">
					<?php echo bbn_trim_content( $row->post_content ); ?>
					</div>
					<div class="bbn-notify-action"><a href="#">mark as read</a></div>
				</div>
			</li>
			<?php
		endforeach;
		?>
		</ul>
		<?php
	}
}

